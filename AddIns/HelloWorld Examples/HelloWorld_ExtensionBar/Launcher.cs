﻿using RightNow.AddIns.AddInViews;
using System;
using System.AddIn;
using System.Windows.Forms;

namespace HelloWorld_ExtensionBar
{
    /// <summary>
    /// this add-in allows you to dock controls to the sides of the main UI
    /// </summary>
    [AddIn("Hello World Extension Bar")]
    public class Launcher : IGlobalDockWindowControl
    {
        /// <summary>
        /// group name...appears to do...nothing
        /// i assume this is like other *Name attributes and expects a fully qualified
        /// object name, but since we don't know what any are, and we can't create them... useless
        /// </summary>
        public string GroupName { get; set; }
        
        /// <summary>
        /// the display order of the extension bar add-ins in the group
        /// adding more than one add-in to a group will convert it to a tab set
        /// </summary>
        public int Order
        {
            get { return 1; }
        }

        /// <summary>
        /// context info about the global window control
        /// </summary>
        /// <param name="context"></param>
        public void SetGDWContext(IGDWContext context)
        {
            this.context = context;

            //appears to be the only way to set the title of the group
            context.Title = "Hello World Title";

            //listen for the two additional events exposed through the context
            context.DockingChanged += context_DockingChanged;
            context.SelectedTabChanged += context_SelectedTabChanged;
        }

        /// <summary>
        /// not quite what you'd expect it to do. this is fired when there are multiple add-ins in a group and the selected tab changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void context_SelectedTabChanged(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// store the dock type on our control so it knows how to size itself
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void context_DockingChanged(object sender, EventArgs e)
        {
            control.DockStyle = context.Docking;
        }

        /// <summary>
        /// do not use. any time this key is hit it will fire the event
        /// </summary>
        public Keys Shortcut
        {
            get;
            set;
        }

        /// <summary>
        /// fired when the Shortcut key is pressed. regardless of focus
        /// </summary>
        public void ShortcutActivated()
        {
        }

        /// <summary>
        /// return our UI control
        /// </summary>
        /// <returns></returns>
        public Control GetControl()
        {
            return control;
        }

        /// <summary>
        /// init the control and its properties
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Initialize(IGlobalContext context)
        {
            //create the control and set the default dock
            control = new ExtensionBarControl();
            control.DockStyle = RightNow.AddIns.Common.DockingType.Top;

            //store the context for later
            this.globalContext = context;

            //init succeeded 
            return true;
        }

        /// <summary>
        /// extension bar context
        /// </summary>
        public IGDWContext context { get; set; }

        /// <summary>
        /// session context
        /// </summary>
        public IGlobalContext globalContext { get; set; }

        /// <summary>
        /// UI control
        /// </summary>
        public ExtensionBarControl control { get; set; }
    }

}
