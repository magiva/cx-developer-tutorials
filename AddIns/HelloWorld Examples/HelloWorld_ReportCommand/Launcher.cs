﻿using HelloWorld_ReportCommand.Properties;
using RightNow.AddIns.AddInViews;
using RightNow.AddIns.Common;
using System.AddIn;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace HelloWorld_ReportCommand
{
    /// <summary>
    /// allows custom actions to be added to reports
    /// </summary>
    [AddIn("HelloWorld ReportCommand")]
    public class Launcher : IReportCommand2
    {
        /// <summary>
        /// return a list of strings to indicate which custom objects this command should be available to
        /// format: [namespace]$[object]
        /// example: CO$Test
        /// </summary>
        public IList<string> CustomObjectRecordTypes
        {
            get
            {
                return new List<string>
                {
                };
            }
        }

        /// <summary>
        /// return true to mark the action as enabled
        /// </summary>
        /// <param name="rows"></param>
        /// <returns></returns>
        public bool Enabled(IList<IReportRow> rows)
        {
            return true;
        }

        /// <summary>
        /// what to do when the link or button is clicked
        /// </summary>
        /// <param name="rows"></param>
        public void Execute(IList<IReportRow> rows)
        {
            MessageBox.Show("Hello World");
        }

        /// <summary>
        /// 16x16 image
        /// </summary>
        public Image Image16
        {
            get { return Resources.ThumbsUp16; }
        }

        /// <summary>
        /// 32x32 image
        /// </summary>
        public Image Image32
        {
            get { return Resources.ThumbsUp32; }
        }

        /// <summary>
        /// list of standard record types that this action should be available to
        /// </summary>
        public IList<ReportRecordIdType> RecordTypes
        {
            get
            {
                return new List<ReportRecordIdType>
                {
                    ReportRecordIdType.Incident,
                    ReportRecordIdType.Contact
                };
            }
        }

        /// <summary>
        /// text to show on the link/button
        /// </summary>
        public string Text
        {
            get { return "Hello World Report Command"; }
        }

        /// <summary>
        /// tooltip for the action
        /// </summary>
        public string Tooltip
        {
            get { return "Hello World Report Command Tooltip"; }
        }
        
        /// <summary>
        /// init the command
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Initialize(IGlobalContext context)
        {
            return true;
        }
    }
}
