﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace StatusToast
{
    public partial class ToastPanel : Form
    {
        /// <summary>
        /// X position
        /// </summary>
        private int startPosX;

        /// <summary>
        /// X offset
        /// </summary>
        private int xOffset = 0;

        /// <summary>
        /// Y position
        /// </summary>
        private int startPosY;

        /// <summary>
        /// Y offset
        /// </summary>
        private int yOffset = 0;

        /// <summary>
        /// where the toast should pop to
        /// </summary>
        private int maxHeight;

        /// <summary>
        /// how tall the control should be
        /// </summary>
        private int formHeight = 0;

        /// <summary>
        /// how far the toast has popped so far
        /// </summary>
        private int currentOffset = 0;

        /// <summary>
        /// the parent form, used for position
        /// </summary>
        private Form parent;

        /// <summary>
        /// default constructor
        /// </summary>
        public ToastPanel()
        {
            InitializeComponent();

            // toast doesn't need to be shown in task bar
            ShowInTaskbar = false;

            // We want our window to be the top most
            TopMost = true;
        }
        
        /// <summary>
        /// show the form as a toast notification
        /// </summary>
        /// <param name="parent">the parent form, we'll use this as a position guide</param>
        /// <param name="xOffset">amount to offset in the X direction</param>
        /// <param name="yOffset">amount to offset in the Y direction</param>
        public void Toast(Form parent, int xOffset = 6, int yOffset = 6)
        {
            this.Show();

            //store values
            this.parent = parent;
            this.xOffset = xOffset;
            this.yOffset = yOffset;
            this.formHeight = Height;

            //listen to visibility changes to the parent form
            parent.SizeChanged += parent_SizeChanged;
            parent.LocationChanged += parent_LocationChanged;
            parent.VisibleChanged += parent_VisibleChanged;

            //set the starting location
            SetDesktopLocation(parent.Width, parent.Height);

            //set our defaults
            this.Height = 0;
            currentOffset = 0;

            //begin animation
            timer1.Start();
        }

        /// <summary>
        /// the parent form has changed visibility, match it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void parent_VisibleChanged(object sender, EventArgs e)
        {
            this.Visible = parent.Visible;
        }

        /// <summary>
        /// the parent form has moved, move with it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void parent_LocationChanged(object sender, EventArgs e)
        {
            int xMax = (parent.Width + parent.Location.X) - xOffset - Width;
            int yMax = (parent.Height + parent.Location.Y) - yOffset - Height;
            this.Location = new Point(xMax, yMax);
        }

        /// <summary>
        /// the parent form has changed size, change position to match it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void parent_SizeChanged(object sender, EventArgs e)
        {
            int xMax = (parent.Width + parent.Location.X) - xOffset - Width;
            int yMax = (parent.Height + parent.Location.Y) - yOffset - Height;
            this.Location = new Point(xMax, yMax);
        }

        /// <summary>
        /// animation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            //change this to change how many pixels are added per tick
            currentOffset += 1;
            
            //figure out our corner
            int xMax = (parent.Width + parent.Location.X) - xOffset;
            int yMax = (parent.Height + parent.Location.Y) - yOffset;

            // Move window out of screen
            startPosX = xMax - Width;
            startPosY = yMax - currentOffset;
            maxHeight = yMax - formHeight;

            //if window is fully visible, within our form, stop the timer
            if (startPosY < maxHeight)
            {
                timer1.Stop();
            }
            else
            {
                //move the form
                this.Height = formHeight - (startPosY - maxHeight);
                this.Location = new Point(startPosX, startPosY);
            }
        }

        /// <summary>
        /// click to close
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToastPanel_MouseClick(object sender, MouseEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// click to close
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}