﻿/****************************
 * 
 * Written by:  Jack Whitehouse @ 45 North
 * Date:        5/22/14
 * Purpose:     Demonstration of the different types of IEvent Add-Ins
 * 
 ****************************/

using RightNow.AddIns.AddInViews;
using System.AddIn;
using System.Windows.Forms;

namespace Events
{
	[AddIn("Console Open")]
	public class ConsoleOpen : IEventConsoleOpen
	{
		public bool Initialize(IGlobalContext context)
		{
			context.LogMessage("--------------------------------------IEventConsoleOpen");
			return true;
		}
	}

	[AddIn("Console Close")]
	public class ConsoleClose : IEventConsoleClose
	{
		public bool Initialize(IGlobalContext context)
		{
			this.context = context;
			return true;
		}

		public void Notify(CloseReason reason)
		{
			context.LogMessage("--------------------------------------IEventConsoleClose - reason: " + reason);
			MessageBox.Show("Close");
		}

		public IGlobalContext context { get; set; }
	}

	[AddIn("Login")]
	public class Login : IEventLogin
	{
		public bool Initialize(IGlobalContext context)
		{
			context.LogMessage("--------------------------------------IEventLogin");
			return true;
		}
	}

	[AddIn("Logout")]
	public class Logout : IEventLogout
	{
		public bool Initialize(IGlobalContext context)
		{
			this.context = context;
			return true;
		}

		public void Notify()
		{
			context.LogMessage("--------------------------------------IEventLogout");
			MessageBox.Show("Logout");
		}

		public IGlobalContext context { get; set; }
	}

	[AddIn("Deleting")]
	public class Deleting : IEventDeleting2
	{
		public bool Initialize(IGlobalContext context)
		{
			this.context = context;
			return true;
		}

		public bool Notify(int recType, string typeName, long recId)
		{
			context.LogMessage(string.Format("--------------------------------------IEventDeleting2 - recType: {0}, typeName: {1}, recId: {2}", recType, typeName, recId));

			return MessageBox.Show("Allow Delete?","", MessageBoxButtons.YesNo) == DialogResult.Yes;
		}

		public IGlobalContext context { get; set; }
	}

	[AddIn("Deleted")]
	public class Deleted : IEventDeleted2
	{
		public bool Initialize(IGlobalContext context)
		{
			this.context = context;
			return true;
		}

		public void Notify(int recType, string typeName, long recId)
		{
			context.LogMessage(string.Format("--------------------------------------IEventDeleted2 - recType: {0}, typeName: {1}, recId: {2}", recType, typeName, recId));
		}

		public IGlobalContext context { get; set; }
	}
}
