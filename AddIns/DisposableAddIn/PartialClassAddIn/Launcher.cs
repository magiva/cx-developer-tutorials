﻿using RightNow.AddIns.AddInViews;
using System;
using System.AddIn;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartialClassAddIn
{
    [AddIn("Partially Disposable AddIn")]
    public class Launcher : IWorkspaceComponentFactory2
    {
        public IWorkspaceComponent2 CreateControl(bool inDesignMode, IRecordContext context)
        {
            return new WorkspaceControl(inDesignMode, context);
        }

        public System.Drawing.Image Image16
        {
            get;
            set;
        }

        public string Text
        {
            get { return "Partially Disposable AddIn"; }
        }

        public string Tooltip
        {
            get { return "Partially Disposable AddIn"; }
        }

        public bool Initialize(IGlobalContext context)
        {
            return true;
        }
    }
}
