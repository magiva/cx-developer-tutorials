﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RightNow.AddIns.AddInViews;


namespace PartialClassAddIn
{
    public partial class WorkspaceControl : UserControl, IWorkspaceComponent2
    {
        private bool inDesignMode;
        private IRecordContext context;

        public WorkspaceControl()
        {
            InitializeComponent();
        }

        public WorkspaceControl(bool inDesignMode, IRecordContext context)
            : this()
        {
            this.inDesignMode = inDesignMode;
            this.context = context;

            if (!inDesignMode)
            {
                context.DataLoaded += context_DataLoaded;
            }
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (inDesignMode == false)
                context.DataLoaded -= context_DataLoaded;

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        void context_DataLoaded(object sender, EventArgs e)
        {
            MessageBox.Show("Partial Loaded");
        }

        public bool ReadOnly
        {
            get;
            set;
        }

        public void RuleActionInvoked(string actionName)
        {
            throw new NotImplementedException();
        }

        public string RuleConditionInvoked(string conditionName)
        {
            throw new NotImplementedException();
        }

        public Control GetControl()
        {
            return this;
        }

    }
}
