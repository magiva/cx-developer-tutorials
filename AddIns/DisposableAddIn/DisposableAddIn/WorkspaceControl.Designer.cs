﻿namespace DisposableAddIn
{
    partial class WorkspaceControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_count = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label_count
            // 
            this.label_count.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_count.Location = new System.Drawing.Point(0, 0);
            this.label_count.Name = "label_count";
            this.label_count.Size = new System.Drawing.Size(150, 150);
            this.label_count.TabIndex = 0;
            this.label_count.Text = "Disposable Demo";
            this.label_count.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WorkspaceControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label_count);
            this.Name = "WorkspaceControl";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label_count;
    }
}
