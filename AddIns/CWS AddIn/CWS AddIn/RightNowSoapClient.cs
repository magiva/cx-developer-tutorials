﻿using CWS_AddIn.RNT_SOAP;
using RightNow.AddIns.AddInViews;
using System;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace CWS_AddIn
{
    /// <summary>
    /// singleton class for the RightNow SOAP Client
    /// </summary>
    public class RightNowSoapClient
    {
        /// <summary>
        /// access to the soap client
        /// </summary>
        public RightNowSyncPortClient client { get; set; }

        /// <summary>
        /// soap header info
        /// </summary>
        public ClientInfoHeader headerInfo { get; set; }

        /// <summary>
        /// true if the connection has been initialized
        /// </summary>
        public bool IsConnected { get; set; }


        #region singleton
        /// <summary>
        /// singleton initializer using the Lazy<<> class
        /// </summary>
        private static readonly Lazy<RightNowSoapClient> _instance = new Lazy<RightNowSoapClient>(() => new RightNowSoapClient()
        {
            IsConnected = false,

            //-----------header info-------------//
            headerInfo = new ClientInfoHeader()
            {
                AppID = "CWS AddIn"
            }
        });

        /// <summary>
        /// singleton access
        /// </summary>
        public static RightNowSoapClient Instance { get { return _instance.Value; } }
        #endregion

        /// <summary>
        /// create the connection based on the global context
        /// </summary>
        /// <param name="context">global context of the site you're connecting to</param>
        internal void Init(IGlobalContext context)
        {
            try
            {
                //get the end point address from the global context
                EndpointAddress endPointAddr = new EndpointAddress(context.GetInterfaceServiceUrl(ConnectServiceType.Soap));

                //create endpoint binding rules
                BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential);
                binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName;
                binding.ReceiveTimeout = new TimeSpan(0, 10, 0);
                binding.MaxReceivedMessageSize = 1048576; //1MB
                binding.SendTimeout = new TimeSpan(0, 10, 0);

                //create the client
                client = new RightNowSyncPortClient(binding, endPointAddr);

                //configure element timestamps
                BindingElementCollection elements = client.Endpoint.Binding.CreateBindingElements();
                elements.Find<SecurityBindingElement>().IncludeTimestamp = false;
                client.Endpoint.Binding = new CustomBinding(elements);

                //set up the client to use session auth based on the current user
                context.PrepareConnectSession(client.ChannelFactory);

                //mark the class as initialized
                IsConnected = true;
            }
            catch (Exception ex)
            {
                IsConnected = false;
                throw ex;
            }
        }

    }
}
