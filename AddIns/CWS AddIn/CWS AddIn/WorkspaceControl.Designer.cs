﻿namespace CWS_AddIn
{
    partial class WorkspaceControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label createdTimeLabel;
            System.Windows.Forms.Label displayNameLabel;
            System.Windows.Forms.Label loginLabel;
            System.Windows.Forms.Label firstLabel;
            System.Windows.Forms.Label lastLabel;
            this.accountBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.createdTimeDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.displayNameTextBox = new System.Windows.Forms.TextBox();
            this.loginTextBox = new System.Windows.Forms.TextBox();
            this.firstTextBox = new System.Windows.Forms.TextBox();
            this.lastTextBox = new System.Windows.Forms.TextBox();
            createdTimeLabel = new System.Windows.Forms.Label();
            displayNameLabel = new System.Windows.Forms.Label();
            loginLabel = new System.Windows.Forms.Label();
            firstLabel = new System.Windows.Forms.Label();
            lastLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.accountBindingSource)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // accountBindingSource
            // 
            this.accountBindingSource.DataSource = typeof(CWS_AddIn.RNT_SOAP.Account);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(lastLabel, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lastTextBox, 1, 4);
            this.tableLayoutPanel1.Controls.Add(firstLabel, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.firstTextBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(loginLabel, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.loginTextBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(displayNameLabel, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.displayNameTextBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(createdTimeLabel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.createdTimeDateTimePicker, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(445, 209);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // createdTimeLabel
            // 
            createdTimeLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            createdTimeLabel.AutoSize = true;
            createdTimeLabel.Location = new System.Drawing.Point(5, 6);
            createdTimeLabel.Name = "createdTimeLabel";
            createdTimeLabel.Size = new System.Drawing.Size(73, 13);
            createdTimeLabel.TabIndex = 0;
            createdTimeLabel.Text = "Created Time:";
            // 
            // createdTimeDateTimePicker
            // 
            this.createdTimeDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.accountBindingSource, "CreatedTime", true));
            this.createdTimeDateTimePicker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.createdTimeDateTimePicker.Enabled = false;
            this.createdTimeDateTimePicker.Location = new System.Drawing.Point(84, 3);
            this.createdTimeDateTimePicker.Name = "createdTimeDateTimePicker";
            this.createdTimeDateTimePicker.Size = new System.Drawing.Size(358, 20);
            this.createdTimeDateTimePicker.TabIndex = 1;
            // 
            // displayNameLabel
            // 
            displayNameLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            displayNameLabel.AutoSize = true;
            displayNameLabel.Location = new System.Drawing.Point(3, 32);
            displayNameLabel.Name = "displayNameLabel";
            displayNameLabel.Size = new System.Drawing.Size(75, 13);
            displayNameLabel.TabIndex = 2;
            displayNameLabel.Text = "Display Name:";
            // 
            // displayNameTextBox
            // 
            this.displayNameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.accountBindingSource, "DisplayName", true));
            this.displayNameTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.displayNameTextBox.Enabled = false;
            this.displayNameTextBox.Location = new System.Drawing.Point(84, 29);
            this.displayNameTextBox.Name = "displayNameTextBox";
            this.displayNameTextBox.Size = new System.Drawing.Size(358, 20);
            this.displayNameTextBox.TabIndex = 3;
            // 
            // loginLabel
            // 
            loginLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            loginLabel.AutoSize = true;
            loginLabel.Location = new System.Drawing.Point(42, 58);
            loginLabel.Name = "loginLabel";
            loginLabel.Size = new System.Drawing.Size(36, 13);
            loginLabel.TabIndex = 4;
            loginLabel.Text = "Login:";
            // 
            // loginTextBox
            // 
            this.loginTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.accountBindingSource, "Login", true));
            this.loginTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loginTextBox.Enabled = false;
            this.loginTextBox.Location = new System.Drawing.Point(84, 55);
            this.loginTextBox.Name = "loginTextBox";
            this.loginTextBox.Size = new System.Drawing.Size(358, 20);
            this.loginTextBox.TabIndex = 5;
            // 
            // firstLabel
            // 
            firstLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            firstLabel.AutoSize = true;
            firstLabel.Location = new System.Drawing.Point(49, 84);
            firstLabel.Name = "firstLabel";
            firstLabel.Size = new System.Drawing.Size(29, 13);
            firstLabel.TabIndex = 6;
            firstLabel.Text = "First:";
            // 
            // firstTextBox
            // 
            this.firstTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.accountBindingSource, "Name.First", true));
            this.firstTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.firstTextBox.Enabled = false;
            this.firstTextBox.Location = new System.Drawing.Point(84, 81);
            this.firstTextBox.Name = "firstTextBox";
            this.firstTextBox.Size = new System.Drawing.Size(358, 20);
            this.firstTextBox.TabIndex = 7;
            // 
            // lastLabel
            // 
            lastLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            lastLabel.AutoSize = true;
            lastLabel.Location = new System.Drawing.Point(48, 110);
            lastLabel.Name = "lastLabel";
            lastLabel.Size = new System.Drawing.Size(30, 13);
            lastLabel.TabIndex = 8;
            lastLabel.Text = "Last:";
            // 
            // lastTextBox
            // 
            this.lastTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.accountBindingSource, "Name.Last", true));
            this.lastTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lastTextBox.Enabled = false;
            this.lastTextBox.Location = new System.Drawing.Point(84, 107);
            this.lastTextBox.Name = "lastTextBox";
            this.lastTextBox.Size = new System.Drawing.Size(358, 20);
            this.lastTextBox.TabIndex = 9;
            // 
            // WorkspaceControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "WorkspaceControl";
            this.Size = new System.Drawing.Size(445, 209);
            ((System.ComponentModel.ISupportInitialize)(this.accountBindingSource)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource accountBindingSource;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox lastTextBox;
        private System.Windows.Forms.TextBox firstTextBox;
        private System.Windows.Forms.TextBox loginTextBox;
        private System.Windows.Forms.TextBox displayNameTextBox;
        private System.Windows.Forms.DateTimePicker createdTimeDateTimePicker;
    }
}
