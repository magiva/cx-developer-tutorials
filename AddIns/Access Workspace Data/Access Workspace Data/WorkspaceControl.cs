﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RightNow.AddIns.AddInViews;
using RightNow.AddIns.Common;

namespace Workspace_Data
{
    public partial class WorkspaceControl : UserControl
    {
        #region private vars
        /// <summary>
        /// true if we're on a workspace designer
        /// </summary>
        private bool inDesignMode;

        /// <summary>
        /// information about the workspace
        /// </summary>
        private IRecordContext recordContext;

        /// <summary>
        /// information about the session
        /// </summary>
        private IGlobalContext globalContext;

        /// <summary>
        /// information about the incident record
        /// </summary>
        private IIncident incidentRecord; 
        #endregion

        /// <summary>
        /// default constructor, used by UI designer
        /// </summary>
        public WorkspaceControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// overloaded constructor takes all of the context related information to be used later
        /// </summary>
        /// <param name="inDesignMode">true if we're on a workspace designer</param>
        /// <param name="recordContext">information about the workspace</param>
        /// <param name="globalContext">information about the session</param>
        /// <remarks>calls default constructor to create UI</remarks>
        public WorkspaceControl(bool inDesignMode, IRecordContext recordContext, IGlobalContext globalContext) : this()
        {
            this.inDesignMode = inDesignMode;
            this.recordContext = recordContext;
            this.globalContext = globalContext;
        }

        /// <summary>
        /// the workspace has finished loading the data, get a reference to the incident data
        /// </summary>
        internal void LoadData()
        {
            //check that we're on the correct type of workspace
            if (recordContext.WorkspaceType == WorkspaceRecordType.Incident)
            {
                //grab the incident
                incidentRecord = recordContext.GetWorkspaceRecord(WorkspaceRecordType.Incident) as IIncident;
            }
            else
            {
                //otherwise disable the button
                button_assign.Enabled = false;
            }
        }

        /// <summary>
        /// 'assign to me' button has been clicked. set the assigned field to the current account
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_assign_Click(object sender, EventArgs e)
        {
            if (incidentRecord != null)
            {
                incidentRecord.Assigned.AcctID = globalContext.AccountId;
            }
        }
    }
}
