﻿/****************************
 * 
 * Written by:  Jack Whitehouse @ 45 North
 * Date:        1/21/14
 * Purpose:     demonstrates workspace control resizing and a known workaround
 * 
 ****************************/
using RightNow.AddIns.AddInViews;
using System;
using System.Windows.Forms;

namespace ControlResizing
{
    public partial class WorkspaceControl : UserControl, IWorkspaceComponent2
    {
        /// <summary>
        /// default constructor
        /// </summary>
        public WorkspaceControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// workspace config. this value will determine the width of the label
        /// </summary>
        [WorkspaceConfigProperty("100",
            Description = "The size of the label in pixels",
            ToolTip = "Must be greater than 0",
            NameInDesigner = "Label Width")]
        public int LabelWidth
        {
            get
            {
                return label1.Width;
            }
            set
            {
                label1.Width = value;
            }
        }

        /// <summary>
        /// whenever the label width changes log it so we can watch the chaos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label1_SizeChanged(object sender, EventArgs e)
        {
            //add the entry
            textBox1.Text += DateTime.Now.ToString("ss.fff") + ": " + label1.Width + Environment.NewLine;

            //scroll to the end so that it can still be seen on a workspace designer
            if (!IsDisposed)
            {
                textBox1.SelectionStart = textBox1.Text.Length;
                textBox1.ScrollToCaret();
            }
        }

        /// <summary>
        /// by overloading this we can ensure that the workspace doesn't try to rescale our control (and throw off our width)
        /// disable this to see the undesired behavior
        /// </summary>
        protected override bool ScaleChildren
        {
            get
            {
                return false;
            }
        }

        #region workspace component properties
        public bool ReadOnly
        {
            get;
            set;
        }

        public void RuleActionInvoked(string actionName)
        {
            throw new NotImplementedException();
        }

        public string RuleConditionInvoked(string conditionName)
        {
            throw new NotImplementedException();
        }

        public Control GetControl()
        {
            return this;
        }
        #endregion
    }
}
