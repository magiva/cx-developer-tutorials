<?php

/**
 * CPMObjectEventHandler: MinimalContactHandler
 * Package: RN
 * Objects: Contact
 * Actions: Create, Update
 * Version: 1.2
 * Purpose: Minimal CPM handler for contact create and update.
 */
use \RightNow\Connect\v1_2 as RNCPHP;
use \RightNow\CPM\v1 as RNCPM;

/**
 * Handler class for CPM
 */
class MinimalContactHandler implements RNCPM\ObjectEventHandler
{

    /**
     * Apply CPM logic to object.
     * @param int $runMode
     * @param int $action
     * @param object $contact
     * @param int $cycles
     */
    public static function apply($runMode, $action, $contact, $cycle)
    {
        if ($cycle !== 0) return;
        
        if (RNCPM\ActionUpdate == $action)
        {
            $contact->Name->First = "CPM Update Test";
            $contact->save();
        }
        elseif (RNCPM\ActionCreate == $action)
        {
            $contact->Name->First = "CPM Create Test";
            $contact->save();
        }
    }

}

/**
 * CPM test harness
 */
class MinimalContactHandler_TestHarness
        implements RNCPM\ObjectEventHandler_TestHarness
{

    static $contactOneId = null,
            $contactTwoId = null;

    /**
     * Set up test cases.
     */
    public static function setup()
    {
        // First test
        $contactOne = new RNCPHP\Contact;
        $contactOne->Name->First = "First";
        $contactOne->save();
        self::$contactOneId = $contactOne->ID;

        // Second test
        $contactTwo = new RNCPHP\Contact;
        $contactTwo->Name->First = "Second";
        $contactTwo->save();
        self::$contactTwoId = $contactTwo->ID;
    }

    /**
     * Return the object that we want to test with. You could also return 
     * an array of objects to test more than one variation of an object.
     * @param int $action
     * @param class $object_type
     * @return object | array
     */
    public static function fetchObject($action, $object_type)
    {
        $contactOne = $object_type::fetch(self::$contactOneId);
        $contactTwo = $object_type::fetch(self::$contactTwoId);
        return array($contactOne, $contactTwo);
    }

    /**
     * Validate test cases
     * @param int $action
     * @param object $contact
     * @return bool
     */
    public static function validate($action, $contact)
    {
        if (RNCPM\ActionUpdate == $action)
        {
            if (assert($contact->Name->First == "CPM Update Test"))
            {
                echo "Update test passed\n";
                return true;
            }
            else
            {
                echo "Update test FAILED\n";
            }
        }
        elseif (RNCPM\ActionCreate == $action)
        {
            if (assert($contact->Name->First == "CPM Create Test"))
            {
                echo "Create test passed\n";
                return true;
            }
            else
            {
                echo "Create test FAILED\n";
            }
        }
        else
        {
            echo "Invalid action";
        }
        return false;
    }

    /**
     * Destroy every object created by this test. Not necessary since in 
     * test mode and nothing is committed, but good practice if only to
     * document the side effects of this test.
     */
    public static function cleanup()
    {
        if (self::$contactOneId)
        {
            $contactOne = RNCPHP\Contact::fetch(self::$contactOneId);
            $contactOne->destroy();
            self::$contactOneId = null;
        }

        if (self::$contactTwoId)
        {
            $contactTwo = RNCPHP\Contact::fetch(self::$contactTwoId);
            $contactTwo->destroy();
            self::$contactTwoId = null;
        }
    }

}

?>
