﻿namespace SOAP_Account_PasswordReset
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_pwd = new System.Windows.Forms.Button();
            this.button_reset = new System.Windows.Forms.Button();
            this.button_all = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_pwd
            // 
            this.button_pwd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_pwd.Location = new System.Drawing.Point(3, 3);
            this.button_pwd.Name = "button_pwd";
            this.button_pwd.Size = new System.Drawing.Size(233, 23);
            this.button_pwd.TabIndex = 0;
            this.button_pwd.Text = "Set Password";
            this.button_pwd.UseVisualStyleBackColor = true;
            this.button_pwd.Click += new System.EventHandler(this.button_pwd_Click);
            // 
            // button_reset
            // 
            this.button_reset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_reset.Location = new System.Drawing.Point(3, 32);
            this.button_reset.Name = "button_reset";
            this.button_reset.Size = new System.Drawing.Size(233, 23);
            this.button_reset.TabIndex = 1;
            this.button_reset.Text = "Set Reset";
            this.button_reset.UseVisualStyleBackColor = true;
            this.button_reset.Click += new System.EventHandler(this.button_reset_Click);
            // 
            // button_all
            // 
            this.button_all.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_all.Location = new System.Drawing.Point(3, 61);
            this.button_all.Name = "button_all";
            this.button_all.Size = new System.Drawing.Size(233, 23);
            this.button_all.TabIndex = 2;
            this.button_all.Text = "Set Password and Reset";
            this.button_all.UseVisualStyleBackColor = true;
            this.button_all.Click += new System.EventHandler(this.button_all_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.button_pwd, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.button_all, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.button_reset, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(239, 126);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(239, 126);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_pwd;
        private System.Windows.Forms.Button button_reset;
        private System.Windows.Forms.Button button_all;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}

