﻿/****************************
 * 
 * Written by:  Jack Whitehouse @ 45 North
 * Date:        12/12/13
 * Purpose:     Test CustomFieldExtension class
 * 
 ****************************/
using EasyCustomFields.Properties;
using EasyCustomFields.RightNow.Soap;
using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Windows.Forms;

namespace EasyCustomFields
{
    public partial class Form1 : Form
    {
        private RightNowSyncPortClient client;
        private ClientInfoHeader headerInfo;

        public Form1()
        {
            InitializeComponent();

            headerInfo = new ClientInfoHeader
            {
                AppID = "Custom Field test app"
            };
        }

        private void button_getIncidentField_Click(object sender, EventArgs e)
        {
            Incident template = GetIncident();

            //populate the field or attribute based on which button was pushed
            if (sender == button_getField)
            {
                textBox_result.Text = (template.GetCustomField("c$stringField") ?? "").ToString();
            }
            else if (sender == button_getIncidentAttr)
            {
                textBox_incidentAttrResult.Text = (template.GetCustomField("CO$stringAttribute") ?? "").ToString();
            }
        }

        private void button_generate_Click(object sender, EventArgs e)
        {
            //set up the soap connection
            InitClient(Settings.Default.site, Settings.Default.username, textBox_pwd.Text);

            Incident template = new Incident
            {
                ID = new ID
                {
                    id = Settings.Default.incidentID,
                    idSpecified = true
                },
                CustomFields = new GenericObject(),
            };

            //set the field based on which button was hit
            if (sender == button_setIncidentField)
            {
                template.SetCustomField("c$stringField", textBox_value.Text, DataTypeEnum.STRING);
            }
            else if (sender == button_setIncidentAttr)
            {
                template.SetCustomField("CO$stringAttribute", textBox_incidentAttrValue.Text, DataTypeEnum.STRING);
            }

            //push the update
            client.Update(headerInfo, new RNObject[] { template }, new UpdateProcessingOptions { SuppressExternalEvents = true, SuppressRules = true });

            MessageBox.Show("Field Updated");
        }


        private void button_runAllTests_Click(object sender, EventArgs e)
        {
            /*
             * this runs the same tests as above, it just does it for each object type to ensure we've got full coverage
             */

            InitClient(Settings.Default.site, Settings.Default.username, textBox_pwd.Text);

            Incident setTemplate = new Incident
            {
                ID = new ID
                {
                    id = Settings.Default.incidentID,
                    idSpecified = true
                },
                CustomFields = new GenericObject(),
            };

            int menuAttr = 1;
            int intAttr = 10;
            DateTime dateTimeAttr = DateTime.Now;
            DateTime dateAttr = DateTime.Today;
            bool boolAttr = false;
            string stringAttr = "test";

            setTemplate.SetCustomField("CO$menuAttr", menuAttr, DataTypeEnum.NAMED_ID);
            setTemplate.SetCustomField("CO$intAttr", intAttr, DataTypeEnum.INTEGER);
            setTemplate.SetCustomField("CO$dateTimeAttr", dateTimeAttr, DataTypeEnum.DATETIME);
            setTemplate.SetCustomField("CO$dateAttr", dateAttr, DataTypeEnum.DATE);
            setTemplate.SetCustomField("CO$boolAttribute", boolAttr, DataTypeEnum.BOOLEAN);
            setTemplate.SetCustomField("CO$stringAttribute", stringAttr, DataTypeEnum.STRING);

            client.Update(headerInfo, new RNObject[] { setTemplate }, new UpdateProcessingOptions { SuppressExternalEvents = true, SuppressRules = true });

            //-------------------------------------------------------------------------------

            Incident getTemplate = GetIncident();

            int menuAttrResult = (int)((NamedID)(getTemplate.GetCustomField("CO$menuAttr") ?? new NamedID { ID = new ID { id = 0 } })).ID.id;
            int intAttrResult = (int)(getTemplate.GetCustomField("CO$intAttr") ?? 0);
            DateTime dateTimeAttrResult = (DateTime)(getTemplate.GetCustomField("CO$dateTimeAttr") ?? DateTime.MinValue);
            DateTime dateAttrResult = (DateTime)(getTemplate.GetCustomField("CO$dateAttr") ?? DateTime.MinValue); ;
            bool boolAttrResult = (bool)(getTemplate.GetCustomField("CO$boolAttribute") ?? true);
            string stringAttrResult = (string)(getTemplate.GetCustomField("CO$stringAttribute") ?? "");

            if (menuAttrResult == menuAttr &&
                intAttrResult == intAttr &&
                (dateTimeAttrResult - dateTimeAttr) < new TimeSpan(0, 0, 1) &&
                dateAttrResult == dateAttr &&
                boolAttrResult == boolAttr &&
                stringAttrResult == stringAttr)
            {
                MessageBox.Show("Tests passed");
            }
            else
            {
                MessageBox.Show("Tests failed");
            }
        }

        /// <summary>
        /// init the RNT soap client
        /// </summary>
        /// <param name="site"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        private void InitClient(string site, string username, string password)
        {
            try
            {
                EndpointAddress endPointAddr = new EndpointAddress(site);

                BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.TransportWithMessageCredential);
                binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName;
                binding.ReceiveTimeout = new TimeSpan(0, 10, 0);
                binding.MaxReceivedMessageSize = 10485760; //1MB
                binding.SendTimeout = new TimeSpan(0, 10, 0);

                client = new RightNowSyncPortClient(binding, endPointAddr);
                client.ClientCredentials.UserName.UserName = username;
                client.ClientCredentials.UserName.Password = password;


                BindingElementCollection elements = client.Endpoint.Binding.CreateBindingElements();
                elements.Find<SecurityBindingElement>().IncludeTimestamp = false;

                client.Endpoint.Binding = new CustomBinding(elements);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// download the incident
        /// </summary>
        /// <returns></returns>
        private Incident GetIncident()
        {
            //create the soap connection
            InitClient(Settings.Default.site, Settings.Default.username, textBox_pwd.Text);

            //create a template object, make sure to include CustomFields object 
            Incident template = new Incident
            {
                ID = new ID
                {
                    id = Settings.Default.incidentID,
                    idSpecified = true
                },
                CustomFields = new GenericObject(),
            };

            //get our test object
            template = client.Get(headerInfo, new RNObject[] { template }, new GetProcessingOptions { FetchAllNames = true })[0] as Incident;
            return template;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Default.Save();
        }

    }
}
