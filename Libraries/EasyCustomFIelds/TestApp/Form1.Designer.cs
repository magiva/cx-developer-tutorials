﻿namespace EasyCustomFields
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_value = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button_setIncidentField = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_user = new System.Windows.Forms.TextBox();
            this.textBox_url = new System.Windows.Forms.TextBox();
            this.textBox_pwd = new System.Windows.Forms.TextBox();
            this.button_getField = new System.Windows.Forms.Button();
            this.textBox_result = new System.Windows.Forms.TextBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.button_getIncidentAttr = new System.Windows.Forms.Button();
            this.button_setIncidentAttr = new System.Windows.Forms.Button();
            this.textBox_incidentAttrResult = new System.Windows.Forms.TextBox();
            this.textBox_incidentAttrValue = new System.Windows.Forms.TextBox();
            this.button_runAllTests = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.textBox_value, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.button_setIncidentField, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox_user, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBox_url, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox_pwd, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.button_getField, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBox_result, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown1, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.button_getIncidentAttr, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.button_setIncidentAttr, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.textBox_incidentAttrResult, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.textBox_incidentAttrValue, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.button_runAllTests, 0, 6);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(636, 312);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 99);
            this.label4.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.label4.Size = new System.Drawing.Size(62, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "Incident ID:";
            // 
            // textBox_value
            // 
            this.textBox_value.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_value.Location = new System.Drawing.Point(84, 150);
            this.textBox_value.Name = "textBox_value";
            this.textBox_value.Size = new System.Drawing.Size(231, 20);
            this.textBox_value.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Endpoint:";
            // 
            // button_setIncidentField
            // 
            this.button_setIncidentField.Location = new System.Drawing.Point(3, 150);
            this.button_setIncidentField.Name = "button_setIncidentField";
            this.button_setIncidentField.Size = new System.Drawing.Size(75, 22);
            this.button_setIncidentField.TabIndex = 1;
            this.button_setIncidentField.Text = "Set Field";
            this.button_setIncidentField.UseVisualStyleBackColor = true;
            this.button_setIncidentField.Click += new System.EventHandler(this.button_generate_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Username:";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Password:";
            // 
            // textBox_user
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.textBox_user, 3);
            this.textBox_user.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::EasyCustomFields.Properties.Settings.Default, "username", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox_user.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_user.Location = new System.Drawing.Point(84, 29);
            this.textBox_user.Name = "textBox_user";
            this.textBox_user.Size = new System.Drawing.Size(549, 20);
            this.textBox_user.TabIndex = 4;
            this.textBox_user.Text = global::EasyCustomFields.Properties.Settings.Default.username;
            // 
            // textBox_url
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.textBox_url, 3);
            this.textBox_url.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::EasyCustomFields.Properties.Settings.Default, "site", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBox_url.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_url.Location = new System.Drawing.Point(84, 3);
            this.textBox_url.Name = "textBox_url";
            this.textBox_url.Size = new System.Drawing.Size(549, 20);
            this.textBox_url.TabIndex = 5;
            this.textBox_url.Text = global::EasyCustomFields.Properties.Settings.Default.site;
            // 
            // textBox_pwd
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.textBox_pwd, 3);
            this.textBox_pwd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_pwd.Location = new System.Drawing.Point(84, 55);
            this.textBox_pwd.Name = "textBox_pwd";
            this.textBox_pwd.PasswordChar = '*';
            this.textBox_pwd.Size = new System.Drawing.Size(549, 20);
            this.textBox_pwd.TabIndex = 0;
            // 
            // button_getField
            // 
            this.button_getField.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button_getField.Location = new System.Drawing.Point(3, 121);
            this.button_getField.Name = "button_getField";
            this.button_getField.Size = new System.Drawing.Size(75, 23);
            this.button_getField.TabIndex = 1;
            this.button_getField.Text = "Get Field";
            this.button_getField.UseVisualStyleBackColor = true;
            this.button_getField.Click += new System.EventHandler(this.button_getIncidentField_Click);
            // 
            // textBox_result
            // 
            this.textBox_result.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_result.Location = new System.Drawing.Point(84, 121);
            this.textBox_result.Name = "textBox_result";
            this.textBox_result.ReadOnly = true;
            this.textBox_result.Size = new System.Drawing.Size(231, 20);
            this.textBox_result.TabIndex = 9;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.numericUpDown1, 3);
            this.numericUpDown1.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::EasyCustomFields.Properties.Settings.Default, "incidentID", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.numericUpDown1.Location = new System.Drawing.Point(84, 95);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(549, 20);
            this.numericUpDown1.TabIndex = 11;
            this.numericUpDown1.Value = global::EasyCustomFields.Properties.Settings.Default.incidentID;
            // 
            // button_getIncidentAttr
            // 
            this.button_getIncidentAttr.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button_getIncidentAttr.Location = new System.Drawing.Point(321, 121);
            this.button_getIncidentAttr.Name = "button_getIncidentAttr";
            this.button_getIncidentAttr.Size = new System.Drawing.Size(75, 23);
            this.button_getIncidentAttr.TabIndex = 1;
            this.button_getIncidentAttr.Text = "Get Attribute";
            this.button_getIncidentAttr.UseVisualStyleBackColor = true;
            this.button_getIncidentAttr.Click += new System.EventHandler(this.button_getIncidentField_Click);
            // 
            // button_setIncidentAttr
            // 
            this.button_setIncidentAttr.Location = new System.Drawing.Point(321, 150);
            this.button_setIncidentAttr.Name = "button_setIncidentAttr";
            this.button_setIncidentAttr.Size = new System.Drawing.Size(75, 22);
            this.button_setIncidentAttr.TabIndex = 1;
            this.button_setIncidentAttr.Text = "Set Attribute";
            this.button_setIncidentAttr.UseVisualStyleBackColor = true;
            this.button_setIncidentAttr.Click += new System.EventHandler(this.button_generate_Click);
            // 
            // textBox_incidentAttrResult
            // 
            this.textBox_incidentAttrResult.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_incidentAttrResult.Location = new System.Drawing.Point(402, 121);
            this.textBox_incidentAttrResult.Name = "textBox_incidentAttrResult";
            this.textBox_incidentAttrResult.ReadOnly = true;
            this.textBox_incidentAttrResult.Size = new System.Drawing.Size(231, 20);
            this.textBox_incidentAttrResult.TabIndex = 9;
            // 
            // textBox_incidentAttrValue
            // 
            this.textBox_incidentAttrValue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_incidentAttrValue.Location = new System.Drawing.Point(402, 150);
            this.textBox_incidentAttrValue.Name = "textBox_incidentAttrValue";
            this.textBox_incidentAttrValue.Size = new System.Drawing.Size(231, 20);
            this.textBox_incidentAttrValue.TabIndex = 0;
            // 
            // button_runAllTests
            // 
            this.button_runAllTests.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tableLayoutPanel1.SetColumnSpan(this.button_runAllTests, 4);
            this.button_runAllTests.Location = new System.Drawing.Point(185, 286);
            this.button_runAllTests.Name = "button_runAllTests";
            this.button_runAllTests.Size = new System.Drawing.Size(266, 23);
            this.button_runAllTests.TabIndex = 12;
            this.button_runAllTests.Text = "Run All Type Tests";
            this.button_runAllTests.UseVisualStyleBackColor = true;
            this.button_runAllTests.Click += new System.EventHandler(this.button_runAllTests_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(636, 312);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_user;
        private System.Windows.Forms.TextBox textBox_url;
        private System.Windows.Forms.TextBox textBox_pwd;
        private System.Windows.Forms.Button button_getField;
        private System.Windows.Forms.TextBox textBox_value;
        private System.Windows.Forms.Button button_setIncidentField;
        private System.Windows.Forms.TextBox textBox_result;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button button_getIncidentAttr;
        private System.Windows.Forms.Button button_setIncidentAttr;
        private System.Windows.Forms.TextBox textBox_incidentAttrResult;
        private System.Windows.Forms.TextBox textBox_incidentAttrValue;
        private System.Windows.Forms.Button button_runAllTests;
    }
}

